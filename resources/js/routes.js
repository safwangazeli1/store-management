import Home from './components/Home'
import Categories from './screens/category/Categories'
import AddCategory from './screens/category/AddCategory'
import EditCategory from './screens/category/EditCategory'
import Shop from './components/Shop'



export default {
    mode: 'history',
    routes: [

        {
            path: '/',
            component: Home
        },

        {
            path: '/admin/categories',
            component: Categories
        },

        {
            path:'/admin/addCategory',
            component: AddCategory
        },

        {
            path:'/admin/editCategory',
            component: EditCategory,
            props: true,
            name: 'EditCategory'
        },
    
        {
            path: '/shop',
            component: Shop,
            name: 'Shop'
        },

    ]
}